from .models.banner import Banner
from .models.city import City
from .models.country import Country
from .models.line import Line
from .models.station import Station
from .models.transport import Transport
from .models.user import User
from .models.alert import Alert
from rest_framework import serializers
from apps.django_rest_captcha.rest_captcha.serializers import RestCaptchaSerializer

class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = ("text", "background_image", "url", "background_color", "text_color")

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name', 'country_id')


class LineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Line
        fields = ('id', 'name', 'city', 'stations', 'transport_id')


class StationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Station
        fields = ('id', 'name', 'longitude', 'latitude')


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name')


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = ('id', 'name')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'city_id')

class CaptchaSerializer(RestCaptchaSerializer):
    class Meta:
        fields = ('captcha_key', 'captcha_value')


class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = ('id', 'description', 'date', 'user_id', 'line_id',
                  'city_id', 'station_id', 'transport_id')
