from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail
from rest_framework.response import Response

@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    try:
        email_plaintext_message = "{}".format(reset_password_token.key)

        send_mail(
            "Token de reseteo de password Memetro",
            email_plaintext_message,
            "noreply@memetro.cat",
            [reset_password_token.user.email]
        )
    except:
        raise ResetPasswordException("Error generating password token")

class ResetPasswordException(Exception):
    pass