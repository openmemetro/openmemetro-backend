from django.db.models.signals import post_save
from django.dispatch import receiver
from django.apps import apps

from api.models.alert import Alert


@receiver(post_save, sender=Alert, dispatch_uid="alert_created")
def create_alert(sender, instance, created, **kwargs):
    if created:
        apps.get_app_config('api').bot.send_alert(instance)

