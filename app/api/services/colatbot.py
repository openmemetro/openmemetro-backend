import logging
from datetime import datetime

from colatbot.colatbot.colatbot import ColatBot
from colatbot.colatbot.constants import WARNING
from colatbot.colatbot.utils import sanitize_lines
from colatbot.colatbot.send import Alert

from django.conf import settings
from django.apps import apps

from api.serializers import AlertSerializer

logger = logging.getLogger()


class ColatBotService:
    def __init__(self):
        logger.info("Creating bot!!")
        self.bot = ColatBot(
            token=settings.TELEGRAM_TOKEN,
            cb=self.callback_create_alert,
            chatIds=settings.TELEGRAM_CHAT_IDS,
            bannedMessage=settings.TELEGRAM_BANNED_MESSAGE,
            self_destruct_seconds=settings.TELEGRAM_SELF_DESTRUCT_SECONDS,
            admin_users=settings.TELEGRAM_ADMIN_USER,
        )

    def start_polling(self):
        logger.info("Starting polling!!")
        self.bot.start_polling()

    def send_alert(self, validated_data):
        logger.info("Sending alert to bot")
        alert = self._create_alert(validated_data)
        try:
            self.bot.send_alert(alert, use_callback=False)
        except Exception as exc:
            logger.error(exc)

    @staticmethod
    def callback_create_alert(bot_alert):
        if bot_alert.what is WARNING.MOSQUITOS:
            Transport = apps.get_model(app_label='api', model_name='Transport')
            Station = apps.get_model(app_label='api', model_name='Station')
            Line = apps.get_model(app_label='api', model_name='Line')
            alert = AlertSerializer(data={
                "description": bot_alert.comments or " ",
                "date": bot_alert.when,
                "user_id": 1,
                "city_id": 1,
                "transport_id": Transport.objects.filter(name__icontains=bot_alert.which).first().id,
                "station_id": Station.objects.filter(name__icontains=bot_alert.where2).first().id,
                "line_id" : Line.objects.filter(name__icontains=sanitize_lines(bot_alert.where)).first().id,
                "icon": "Hola Mundo",
            })
            alert.is_valid(raise_exception=True)
            alert.save()
            logger.info("Alert created from Colatbot")
            # except MissingRequiredFieldsException:
            # except ValidationError:
            # except AssertionError:
            # except Exception as e:


    def _create_alert(self, validated_data):
        when    = datetime.now()
        # TODO: Implement what in the app alerts
        what    = WARNING.MOSQUITOS
        which   = validated_data['transport_id'].name
        where   = validated_data['line_id'].name
        where2  = validated_data['station_id'].name
        comments= validated_data['description']
        alert = Alert(
            when,
            what,
            which,
            where,
            where2,
            comments,
        )
        return alert
