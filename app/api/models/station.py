from django.db import models


class Station(models.Model):
    name = models.CharField(max_length=60)
    latitude = models.CharField(max_length=200)
    longitude = models.CharField(max_length=200)

    def __str__(self):
        return self.name
