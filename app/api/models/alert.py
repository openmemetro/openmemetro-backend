from django.db import models
from api.models.city import City
from api.models.station import Station
from api.models.line import Line
from api.models.transport import Transport
from api.models.user import User


class Alert(models.Model):
    description = models.CharField(max_length=60, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length=60)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    city_id = models.ForeignKey(City, on_delete=models.CASCADE, null=True)
    station_id = models.ForeignKey(
        Station, on_delete=models.CASCADE, null=True)
    line_id = models.ForeignKey(Line, on_delete=models.CASCADE, null=True)
    transport_id = models.ForeignKey(
        Transport, on_delete=models.CASCADE, null=True)
