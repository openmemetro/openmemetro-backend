from django.db import models

from django.contrib.auth.models import User as DjangoUser
from .city import City


class User(DjangoUser):
    city_id = models.ForeignKey(City, on_delete=models.CASCADE)

    def __str__(self):
        return self.username
