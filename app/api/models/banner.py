from django.db import models


class Banner(models.Model):
    text = models.CharField(max_length=60)
    background_image = models.TextField(blank=True,)
    url = models.URLField(blank=True)
    background_color = models.CharField(max_length=10, blank=True)
    text_color = models.CharField(max_length=10, default="#000000")
    active = models.BooleanField(default=False)
