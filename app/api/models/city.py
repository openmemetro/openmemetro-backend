from django.db import models
from .country import Country


class City(models.Model):
    name = models.CharField(max_length=60)
    country_id = models.ForeignKey(
        Country, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name
