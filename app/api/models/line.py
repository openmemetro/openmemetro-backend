from django.db import models
from .city import City
from .station import Station
from .transport import Transport


class Line(models.Model):
    name = models.CharField(max_length=60)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    transport_id = models.ForeignKey(
        Transport, null=True, on_delete=models.CASCADE)
    stations = models.ManyToManyField(Station)

    def __str__(self):
        return self.name
