# Generated by Django 4.0.1 on 2022-04-14 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_alert_user_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alert',
            name='description',
            field=models.CharField(blank=True, max_length=60, null=True),
        ),
    ]
