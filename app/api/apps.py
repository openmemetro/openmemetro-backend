import sys

from django.apps import AppConfig
from django.conf import settings


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'

    def ready(self):
        is_runserver = any(arg.casefold() == "runserver" for arg in sys.argv)
        if is_runserver and settings.TELEGRAM_TOKEN:
            from api.services.colatbot import ColatBotService
            self.bot = ColatBotService()
            self.bot.start_polling()

