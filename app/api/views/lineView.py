from rest_framework import viewsets

from ..serializers import LineSerializer
from ..models.line import Line


class LineViewSet(viewsets.ModelViewSet):
    permission_classes = []
    authentication_classes = []
    queryset = Line.objects.all().order_by('id')
    serializer_class = LineSerializer
