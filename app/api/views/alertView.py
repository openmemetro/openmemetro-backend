from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from ..models.alert import Alert
from ..serializers import AlertSerializer

from datetime import datetime, timedelta
import json
from ..utils.requests import validateRequiredFields, MissingRequiredFieldsException
from django.conf import settings
from django.apps import apps
import logging

logger = logging.getLogger(__name__)

class AlertViewSet(viewsets.ModelViewSet):
    queryset = Alert.objects.all().order_by('id')
    serializer_class = AlertSerializer

    def create(self, request):
        required_fields = ["comment", "city_id",
                           "station_id", "line_id", "transport_id"]
        try:
            validateRequiredFields(required_fields, request.data)
            alert = AlertSerializer(data={
                "description": request.data["comment"],
                "date": datetime.utcnow(),
                "user_id": int(request.user.id),
                "city_id": request.data["city_id"],
                "station_id": request.data["station_id"],
                "line_id": request.data["line_id"],
                "transport_id": request.data["transport_id"],
            })
            alert.is_valid(raise_exception=True)
            alert.save()
            if settings.TELEGRAM_TOKEN:
                apps.get_app_config('api').bot.send_alert(alert.validated_data)
        except MissingRequiredFieldsException:
            return Response({"message": "Missing Required Fields"}, 400)
        except ValidationError:
            return Response({"message": "Alert can't be created"}, 401)

        except AssertionError as e:
            # Esto es cuando el serializer no chuta.
            logger.error(e)
            return Response({"message": "Bad Request"}, 400)
        except Exception as e:
            logger.error(e)
            return Response("Unknown error", 500)
        return Response({"message": "Alert Created Succesfully"}, status=200)

# TODO: Separate this file in one file per class.


class AlertsViewSet(viewsets.ViewSet):
    permission_classes = []
    authentication_classes = []

    def list(self, request):
        now = datetime.utcnow()
        start_date = now - timedelta(days=1)
        end_date = now - timedelta(days=1)
        alerts_list = [
            AlertSerializer(c).data
            for c in Alert.objects.all().filter(
                date__range=(start_date, now)
            ).order_by("id")
        ]
        return Response(alerts_list, 200)
