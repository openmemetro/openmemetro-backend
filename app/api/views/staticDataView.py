from rest_framework import viewsets
from rest_framework.response import Response

from ..serializers import CitySerializer, CountrySerializer
from ..models.city import City
from ..models.country import Country


class StaticData(viewsets.ViewSet):
    permission_classes = []
    authentication_classes = []
    queryset = City.objects.all().order_by('id')

    def list(self, request):
        staticdata = {"Country": {"data": []}, "City": {"data": []}}
        cities_list = [
            CitySerializer(c).data
            for c in City.objects.all().order_by("id")
        ]
        staticdata["City"]["data"] = cities_list

        countries_list = [
            CountrySerializer(c).data
            for c in Country.objects.all().order_by("id")
        ]
        staticdata["Country"]["data"] = countries_list
        return Response(staticdata)
