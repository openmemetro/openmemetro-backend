from rest_framework import viewsets
from rest_framework.response import Response

from api.models.city import City
from api.models.line import Line
from api.models.transport import Transport
from api.models.station import Station
from api.models.country import Country
from api.serializers import LineSerializer, CitySerializer, StationSerializer, CountrySerializer, TransportSerializer


class SyncronizeViewSet(viewsets.ViewSet):

    # todo: uncomment this to remove authenticated endpoint
    # permission_classes = []
    # authentication_classes = []

    def list(self, request):
        data = dict()
        user = request.user
        if user.id: data.update(self._user_data(user))
        data.update(self._cities_data())
        data.update(self._countries_data())
        data.update(self._lines_data())
        data.update(self._transports_data())
        data.update(self._stations_data())
        data.update(self._citiestransport_data())
        data.update(self._linestations_data())
        data = {"data": data}
        return Response(data)

    def _user_data(self, user):
        user_data = {
            "user": {
                "name": user.get_short_name(),
                "username": user.username,
                "email": user.email,
                # todo: implement more cities
                "city_id": 1
            }
        }
        return user_data

    def _cities_data(self):
        cities_list = [
            CitySerializer(c).data
            for c in City.objects.all().order_by("id")
        ]
        cities_data = {
            "city": {
                "data": cities_list
            }
        }
        return cities_data

    def _countries_data(self):
        countries_list = [
            CountrySerializer(c).data
            for c in Country.objects.all().order_by("id")
        ]
        countries_data = {
            "country": {
                "data": countries_list
            }
        }
        return countries_data

    def _lines_data(self):
        lines_list = [
            LineSerializer(l).data
            for l in Line.objects.all().order_by("id")
        ]
        lines_data = {
            "line": {
                "data": lines_list
            }
        }
        return lines_data

    def _transports_data(self):
        transports_list = [
            TransportSerializer(t).data
            for t in Transport.objects.all().order_by("id")
        ]
        transports_data = {
            "transport": {
                "data": transports_list
            }
        }
        return transports_data

    def _stations_data(self):
        stations_list = [
            StationSerializer(s).data
            for s in Station.objects.all().order_by("id")
        ]
        stations_data = {
            "station": {
                "data": stations_list
            }
        }
        return stations_data

    def _citiestransport_data(self):
        citiestransport_list = []
        for c in City.objects.all().order_by("id"):
            citiestransport_list.extend([
                {
                    "transport_id": s.id,
                    "city_id": c.id,
                }
                for s in Transport.objects.all().order_by("id")
            ])
        citiestransport_data = {
            "citiestransport": {
                "data": citiestransport_list
            }
        }
        return citiestransport_data

    def _linestations_data(self):
        linestations_list = []
        for l in Line.objects.all().order_by("id"):
            linestations_list.extend([
                {
                    "line_id": l.id,
                    "station_id": s.id,
                }
                for s in l.stations.all()
            ])
        linestations_data = {
            "linesstations": {
                "data": linestations_list
            }
        }
        return linestations_data
