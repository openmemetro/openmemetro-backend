from rest_framework import viewsets

from ..serializers import StationSerializer
from ..models.station import Station


class StationViewSet(viewsets.ModelViewSet):
    permission_classes = []
    authentication_classes = []

    queryset = Station.objects.all().order_by('id')
    serializer_class = StationSerializer
