from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from ..models.alert import Alert
from ..models.banner import Banner
from ..serializers import AlertSerializer, BannerSerializer

from datetime import datetime, timedelta


class BannersViewSet(viewsets.ViewSet):
    permission_classes = []
    authentication_classes = []
    def list(self, request):
        banner_list = [
            BannerSerializer(c).data
            for c in Banner.objects.all().filter(
                active=True
            ).order_by("id")
        ]
        return Response(banner_list, 200)
