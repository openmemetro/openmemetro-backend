from threading import local

from django.contrib.auth.hashers import make_password
from django.contrib.auth.password_validation import validate_password
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from django.core.exceptions import ValidationError as DjangoValidationError

from ..serializers import StationSerializer, UserSerializer, CaptchaSerializer
from ..models.city import City
from ..models.user import User
from ..utils.requests import validateRequiredFields, MissingRequiredFieldsException
import django.apps
import logging
from api.models.user import User

logger = logging.getLogger(__name__)


class SignUpViewSet(viewsets.ViewSet):
    permission_classes = []
    authentication_classes = []

    def create(self, request):
        required_fields = ["username", "email", "password", "city_id", "captcha_value", "captcha_key"]
        try:
            validateRequiredFields(required_fields, request.data)

            captcha = CaptchaSerializer(data={
                "captcha_value": request.data["captcha_value"],
                "captcha_key": request.data["captcha_key"],
            })
            captcha.is_valid(raise_exception=True)

            if User.objects.filter(email__iexact=request.data["email"]):
                logger.error("Error: email already taken")
                return Response({"message": "User can't be created"}, 400)
            validate_password(request.data["password"])
            newUser = UserSerializer(data={
                "username": request.data["username"],
                "email": request.data["email"],
                "password": make_password(request.data["password"]),
                "city_id": request.data["city_id"],
            })
            newUser.is_valid(raise_exception=True)
            newUser.save()
        except MissingRequiredFieldsException as e:
            logger.error(e)
            return Response({"message": "Missing Required Fields"}, 400)
        except ValidationError as e:
            logger.error(e)
            if 'captcha' in str(e):
                return Response(e.__dict__, 401)
            return Response({"message": "User can't be created"}, 401)
        except DjangoValidationError as e:
            logger.error(e)
            return Response({"message": "User can't be created"}, 401)
        except Exception as e:
            logger.error(e)
            return Response("Unknown error", 500)
        return Response({"message": "User Created Succesfully"}, 200)
