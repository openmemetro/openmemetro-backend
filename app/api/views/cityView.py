from rest_framework import viewsets

from ..serializers import CitySerializer
from ..models.city import City


class CityViewSet(viewsets.ModelViewSet):
    permission_classes = []
    authentication_classes = []
    queryset = City.objects.all().order_by('id')
    serializer_class = CitySerializer
