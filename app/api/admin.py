from django.contrib import admin

# Register your models here.
from .models.banner import Banner
from .models.city import City
from .models.country import Country
from .models.line import Line
from .models.station import Station
from .models.transport import Transport
from .models.user import User
from .models.alert import Alert

admin.site.register(City)
admin.site.register(Country)
admin.site.register(User)


@admin.register(Banner)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'active', 'text', 'url', 'background_image')


@admin.register(Alert)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user_id', 'description', 'transport_id', 'line_id', 'station_id', 'date')


@admin.register(Transport)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name')


@admin.register(Line)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'transport_id', 'city')


@admin.register(Station)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name')

