from django.urls import include, path
from rest_framework import routers

from .views.bannerView import BannersViewSet
from .views.cityView import CityViewSet
from .views.lineView import LineViewSet
from .views.stationView import StationViewSet
from .views.synchronizeView import SyncronizeViewSet
from .views.alertView import AlertsViewSet, AlertViewSet
from .views.staticDataView import StaticData
from .views.signupView import SignUpViewSet
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)


router = routers.DefaultRouter()
router.register(r'lines', LineViewSet)
router.register(r'citys', CityViewSet)
router.register(r'stations', StationViewSet)
router.register(r'synchronize', SyncronizeViewSet, basename='synchronize')
router.register(r'signup', SignUpViewSet, basename='signup')
router.register(r'alerts', AlertsViewSet, basename='alerts')
router.register(r'alert', AlertViewSet, basename='alert')
router.register(r'banner', BannersViewSet, basename='banner')


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pairs'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('StaticData/', StaticData.as_view({"get": "list"}, name="StaticData")),
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/captcha/', include('apps.django_rest_captcha.rest_captcha.urls')),
]
