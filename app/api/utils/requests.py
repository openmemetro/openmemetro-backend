
def validateRequiredFields(required_fields, recievedFields):
    for field in required_fields:
        if field not in recievedFields.keys():
            raise MissingRequiredFieldsException("missing fields")


class MissingRequiredFieldsException(Exception):
    pass
