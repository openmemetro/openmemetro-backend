import json
file = open("../docs/transports.json", "r")
# file = open("../docs/transports.json", "r")
transports = json.load(file)

transports_raw_dict = transports.copy()

# Transports


def create_transports(raw_t):
    transport_dicts = list()
    transport_refs = dict()
    pk = 1
    for t in raw_t:
        transport_dicts.append({
            "model": "api.transport",
            "pk": pk,
            "fields": {
                "name": t,
            }
        })
        transport_refs[t] = pk
        pk = pk + 1
    return transport_dicts, transport_refs
    # TODO: Create file


raw_transport_names = transports_raw_dict.keys()
transport_dicts, transport_refs = create_transports(raw_transport_names)
with open('api/fixtures/transports.json', 'w') as file:
    json.dump(transport_dicts, file, indent=4)

# Station


def create_stations(t_raw_dict):
    # print("Creating stations!!")
    station_dicts = list()
    stations_name_list = set()

    for trans, t_values in t_raw_dict.items():
        lines = t_values["lines"]
        for line, l_values in lines.items():
            if lines[line]:
                stations_name_list.update(lines[line]["stops"])

    pk = 1
    station_refs = dict()
    for s in stations_name_list:
        station_dicts.append({
            "model": "api.station",
            "pk": pk,
            "fields": {
                "name": s,
            }
        })
        station_refs[s] = pk
        pk = pk + 1
    return station_dicts, station_refs


station_dicts, station_refs = create_stations(transports_raw_dict)
with open('api/fixtures/stations.json', 'w') as file:
    json.dump(station_dicts, file, indent=4)

# Line


def create_line(t_raw_dict, station_refs, transport_refs):
    # print("Creating line!!")
    lines_dict = list()
    pk = 1

    for transport, t_values in t_raw_dict.items():
        lines = t_values["lines"]
        for line, l_values in lines.items():
            if not l_values:
                continue
            stations = list()
            for station in l_values["stops"]:
                stations.append(station_refs[station])
            lines_dict.append({
                "model": "api.line",
                "pk": pk,
                "fields": {
                    "name": line,
                    "city": 1,
                    "stations": stations,
                    "transport_id": transport_refs[transport],
                }
            })
            pk = pk + 1

    return lines_dict


lines_dict = create_line(transports_raw_dict, station_refs, transport_refs)
with open('api/fixtures/lines.json', 'w') as file:
    json.dump(lines_dict, file, indent=4)
