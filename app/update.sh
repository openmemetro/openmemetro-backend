#!/bin/bash 

git pull origin development
python3 manage.py collectstatic
# For first execution may use
# git submodule update --init --recursive
# To get the submodulec
git submodule update --recursive --remote
sudo systemctl restart memetro-api.service
