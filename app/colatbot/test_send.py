from datetime import datetime

from constants import WARNING
from send import TelegramSender, Alert
from settings import SETTINGS
from utils import format_transport_lines

when    = datetime.now()
what    = WARNING.MOSQUITOS
which   = "Metro"
where   = "L3"
where2  = "Fontana"
comments= "Controlako"

sender = TelegramSender(token=SETTINGS['TOKEN'])

alert = Alert(
    when,
    what,
    which,
    where,
    where2,
    comments,
)

sender.send_alert(alert)
