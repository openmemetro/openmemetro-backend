from datetime import datetime

from colatbot.colatbot import ColatBot
from colatbot.constants import WARNING
from colatbot.send import TelegramSender, Alert
from colatbot.settings import SETTINGS
from colatbot.utils import format_transport_lines

# Callback to call when an alert is sent
def callback(a: Alert):
    print("Marvelous Callback")
    print(a.__dict__)

# Instantiate the bot
bot = ColatBot(token=SETTINGS['TOKEN'], cb=callback, chatIds=["-1001377391601"])

# Start polling (listening for events over there)
bot.start_polling()
# If idle activated will stop following executions (the code stops hete)
# bot.idle()

# Create a new Alert
when    = datetime.now()
what    = WARNING.MOSQUITOS
which   = "Metro"
# In order to send messages with formated lines (lines with colors) respect the names on data/transports.json
where   = "L3"
where2  = "Fontana"
comments= "Sending from script"
alert = Alert(
    when,
    what,
    which,
    where,
    where2,
    comments,
)

# And send the alert message
bot.send_alert(alert, use_callback=False)

# Easy pease :)
