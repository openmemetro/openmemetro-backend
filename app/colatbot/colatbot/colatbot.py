import json
import os

from . import constants
from . import utils
from .admin import ADMIN_COMMANDS
from .send import TelegramSender, Alert, Sender
from .state import State, start_cmd, cancel_cmd, initializeStateButtons
from .constants import END, STATES
from .settings import SETTINGS

from telegram import Chat, Update
from telegram.ext import Updater, CommandHandler, ConversationHandler, TypeHandler, DispatcherHandlerStop


def is_group_middleware(bot, update):
    if (update.effective_chat and update.effective_chat.type == 'group' or update.effective_chat.type == 'supergroup'):
        constants.warning_log.warning("Some body tried to talk bot in a chat: \n" + str(update.effective_chat) +
                                      "\n" + str(update.effective_user))
        raise DispatcherHandlerStop


def black_list_middleware(bot, update):
    if update.effective_user \
            and update.effective_user.username in constants.BLACK_LIST or \
            update.effective_user.id in constants.BLACK_LIST:
        constants.warning_log.warning("Blacklisted user tried to use me! : " + str(update.effective_user))
        # This stops any other handlers in higher groups from running
        # Source https://stackoverflow.com/a/67597586/4708664
        chat_id = update.effective_chat.id
        for msg in SETTINGS['BANNED_MESSAGE']: bot.send_message(chat_id, msg, )
        raise DispatcherHandlerStop


def check_admin_middleware(bot, update):
    if update and update.message and update.message.text and update.message.text.replace('/',
                                                                                         '') in ADMIN_COMMANDS.keys():
        if update.effective_user and update.effective_user.username in SETTINGS['ADMIN_LIST'] or \
                update.effective_user.id in SETTINGS['ADMIN_LIST']:
            constants.warning_log.warning("Admin user connected : " + str(update.effective_user))
        else:
            constants.warning_log.warning("Attempt to access admin commands " + str(update.effective_user))
            for msg in SETTINGS['BANNED_MESSAGE']: bot.send_message(update.effective_chat.id, msg, )
            raise DispatcherHandlerStop


class ColatBot(object):
    updater: Updater

    def __init__(self,
                 token: str,
                 cb = None,
                 chatIds = [],
                 bannedMessage = [],
                 self_destruct_seconds = -1,
                 admin_users = []
                 ):

        if chatIds:
            SETTINGS["CHAT_IDS"] = chatIds
        if bannedMessage:
            SETTINGS['BANNED_MESSAGE'] = bannedMessage
        if self_destruct_seconds > 0:
            SETTINGS['SELF_DESTRUCT_SECONDS'] = self_destruct_seconds
        if admin_users:
            SETTINGS['ADMIN_LIST'] = admin_users


        self.token = token
        self.sender = Sender(
            TelegramSender(token=token),
            cb
        )

        # Read transports file
        wrk_dir = os.path.dirname(os.path.abspath(__file__))
        with open(wrk_dir + "/../data/transports.json") as f:
            tr = json.load(f)
        constants.TRANSPORTS = tr

        # Initialize states
        # todo: merge this next instructions into a one. Using a switch case and the WICH,HOW,WHEN (...) enum,
        # we could generate all states on just one iteration
        states = {state_name: State(END, None, END, self.sender)
                  for state_name in ["when", "which", "what", "where", "where2", "how"]}  # todo: move this to an enum
        constants.STATES = states.copy()

        # Initialize state buttons. Buttons matrix, next step, etc...
        initializeStateButtons(constants.STATES)

        # Build the conversation handler with all state objects
        states_adapted = {state.code: [state.handler()] for name, state in constants.STATES.items()}

        # Create the main handler
        main_handler = ConversationHandler(
            entry_points=[CommandHandler("start", start_cmd)],
            states=states_adapted,
            fallbacks=[CommandHandler("cancel", cancel_cmd)],
        )

        # Set and start!
        self.updater = Updater(token=self.token, )

        # Build middlewares
        # Prevent bot to be used on groups
        group_middleware = TypeHandler(Update, is_group_middleware)
        # Build blacklist handler
        utils.read_blacklist()
        black_list_handler = TypeHandler(Update, black_list_middleware)
        # Build check admin middleware
        check_admin_handler = TypeHandler(Update, check_admin_middleware)

        self.updater.dispatcher.add_handler(group_middleware, group=-2)
        self.updater.dispatcher.add_handler(black_list_handler, group=-1)
        self.updater.dispatcher.add_handler(check_admin_handler, group=0)
        self.updater.dispatcher.add_handler(main_handler, group=1)
        for k, v in ADMIN_COMMANDS.items():
            self.updater.dispatcher.add_handler(CommandHandler(k, v['function'], pass_args=v['args']), group=2)

    def start_polling(self):
        self.updater.start_polling()

    def idle(self):
        self.updater.idle()

    def send_alert(self, alert: Alert, use_callback: bool = True):
        self.sender.send_alert(alert, 'localhost', use_callback)
