import json
import subprocess

from .settings import SETTINGS
from . import constants
from .utils import write_blacklist


def admin(bot, update):
    msg = 'Available admin commands:\n\n'
    msg += "\n".join('/' + k + ': ' + v['description'] for k, v in ADMIN_COMMANDS.items())
    bot.send_message(update.effective_chat.id, msg, )


def warning_log(bot, update,  **optional_args):
    """Show last 10 lines of warning log"""
    tail_lines = 10
    if optional_args['args']:
        tail_lines = optional_args['args'][0]
    with subprocess.Popen(r"tail -n " + str(tail_lines) + " " + SETTINGS['WARNING_LOG'], shell=True,
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE) as p:
        output, errors = p.communicate()
    lines = output.decode('utf-8')
    bot.send_message(update.effective_chat.id, lines, )


def show_blacklist(bot, update):
    bot.send_message(update.effective_chat.id, constants.BLACK_LIST, )


def add_blacklist(bot, update, **optional_args):
    constants.info_log.info("Add to blacklist " + str(optional_args['args']))
    for arg in optional_args['args']:
        if arg not in constants.BLACK_LIST:
            constants.BLACK_LIST.append(arg)
    write_blacklist(constants.BLACK_LIST)
    bot.send_message(update.effective_chat.id, constants.BLACK_LIST, )

def remove_blacklist(bot, update, **optional_args):
    constants.info_log.info("Add to blacklist " + str(optional_args['args']))
    for arg in optional_args['args']:
        if arg in constants.BLACK_LIST:
            constants.BLACK_LIST.remove(arg)
            write_blacklist(constants.BLACK_LIST)
            bot.send_message(update.effective_chat.id, constants.BLACK_LIST, )
        else:
            bot.send_message(update.effective_chat.id, "Not in list!", )

ADMIN_COMMANDS = {
    'admin': {'function': admin, 'args': False, 'description': 'Show help'},
    'warning_log': {'function': warning_log, 'args': True, 'description': 'Show warning logs last lines, by default 10'},
    'show_blacklist': {'function': show_blacklist, 'args': False, 'description': 'Show people banned'},
    'unban': {'function': remove_blacklist, 'args': True, 'description': 'Unban from blacklist'},
    'ban': {'function': add_blacklist, 'args': True,
                      'description': 'Add people to banned list using username as arg'},
}
