from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.ext import MessageHandler, CallbackQueryHandler, Filters

from . import constants
from .send import Sender, Alert
from .utils import vector2matrix, get_chat_id, format_transport_lines, sanitize_lines

from datetime import datetime, timedelta
import time
from .settings import SETTINGS

import logging

"""Conversation states"""
WHEN, WHICH, WHAT, WHERE, WHERE2, HOW = range(6)

"""Message to edit to add new queries"""
MESSAGE_ID = ""

def start_cmd(bot, update):
    """
    Redirect to first step
    """
    logging.info("Starting form")
    global MESSAGE_ID
    MESSAGE_ID = bot.send_message(get_chat_id(update), "Comencem!").message_id
    constants.STATES["when"].query(bot, update, None)
    return constants.STATES["when"].code


def cancel_cmd(bot, update):
    """
    Don't save anything and return back to before `/start`
    """
    constants.info_log.info("alert cancelled")
    bot.deleteMessage(get_chat_id(update), MESSAGE_ID)
    return constants.END

"""Store builders for buttons for the warning form

There are text only messages or text with button matrix messages
"""
class QueryBuilder(object):
    # todo: is this inheritance correct? Something strange...
    def __init__(self, qtext, qbuttons=None):
        self.text = qtext
        self.buttons = qbuttons

class TextQueryBuilder(QueryBuilder):
    def __init__(self, qtext: str):
        super().__init__(qtext, None)

def initializeStateButtons(states):
    """Initialize buttons depending of the state of the conversation"""

    # Configure when butttons
    button_array = ButtonQueryBuilder.button_matrix(
        [
            [("Ara", "now"), ("Fa 15 min", -15)],
            [("Fa 30 min", -30), ("Fa 1 hora", -60)]
        ])
    query = ButtonQueryBuilder("Quan ha passat?", button_array)
    states["when"].set(WHEN, query, states["which"])

    # Configure which buttons
    button_array = vector2matrix(list(constants.TRANSPORTS))
    query = ButtonQueryBuilder("A quin mitjà de transport?", ButtonQueryBuilder.button_matrix(button_array))
    states["which"].set(WHICH, query, states["what"])

    # Configure what buttons
    # todo: use enums for the result
    button_array = ButtonQueryBuilder("Què està passant?", ButtonQueryBuilder.button_matrix(
        [
            [("{} Goril·les".format(constants.WARNING.GORILAS.icon), str(constants.WARNING.GORILAS.name)),
             ("{} Mosquits".format(constants.WARNING.MOSQUITOS.icon), str(constants.WARNING.MOSQUITOS.name))],
            [("{} Gossos de quadra".format(constants.WARNING.GOSSOS.icon), str(constants.WARNING.GOSSOS.name)),
             ("{} Vía lliure".format(constants.WARNING.LLIURE.icon), str(constants.WARNING.LLIURE.name))],
            [("{} Incidències".format(constants.WARNING.INCIDENCIA.icon), str(constants.WARNING.INCIDENCIA.name)),
             ("{} Preguntes".format(constants.WARNING.PREGUNTA.icon), str(constants.WARNING.PREGUNTA.name))]
        ]
    ))
    states["what"].set(WHAT, button_array, states["where"])

    # Configure where buttons
    states["where"].set(WHERE, ButtonQueryBuilder("Línia de transport", ButtonQueryBuilder.button_matrix([["dummy"]])),
                        states["where2"])
    states["where2"].set(WHERE2, ButtonQueryBuilder("Parada/Estació", ButtonQueryBuilder.button_matrix([["dummy"]])),
                         states["how"])

    # Configure how buttons
    states["how"].set(HOW, TextQueryBuilder("Algun comentari, consell, o detall més? (o digueu 'No' o 'n')"), None)


class ButtonQueryBuilder(QueryBuilder):
    def __init__(self, qtext, qbuttons: [[InlineKeyboardButton]]):
        super().__init__(qtext, qbuttons)

    @staticmethod
    def button_matrix(values: [[str]], add_all_button: bool = False) -> [[InlineKeyboardButton]]:
        buttons = []
        for line in values:
            button_row = []
            for button in line:
                if type(button) == tuple and len(button) == 2:
                    show, send = button
                    button_row.append(InlineKeyboardButton(show, callback_data=send))
                else:
                    button_row.append(InlineKeyboardButton(button, callback_data=button))

            buttons.append(button_row)

        button_row = []
        if add_all_button:
            button_row.append(InlineKeyboardButton("Totes", callback_data="Totes"))

        button_row.append(InlineKeyboardButton("Canceŀla", callback_data="cancel"))
        buttons.append(button_row)

        return buttons
'''
Functions related to send final message with the warning
'''

def cancel_callback(bot, update, user_data):
    return cancel_cmd(bot, update)

def complete_callback(bot, update, user_data, sender: Sender):
    """Function that sends the final alert to configured chats"""

    # Fallback danger emoji
    def danger_color(danger):
        for warning in constants.WARNING:
            if danger == str(warning.name):
                return constants.WARNING[danger]
        return constants.WARNING.DEFAULT


    # Datetime
    s_when = user_data[WHEN]
    t_when = datetime.now()
    if s_when != "now":
        delta = timedelta(minutes=int(s_when))
        t_when = t_when + delta

    # What object
    what_warning = danger_color(user_data[WHAT]),

    # Create the sender object
    alert = Alert(
        t_when,
        what_warning[0],
        user_data[WHICH],
        user_data[WHERE],
        user_data[WHERE2],
        user_data[HOW] if user_data[HOW].lower() != 'no' and user_data[HOW].lower() != 'n' else ""
    )

    # Send!
    sender.send_alert(alert, update.effective_user)
    bot.edit_message_text(chat_id=get_chat_id(update), message_id=MESSAGE_ID, text="Gràcies per contribuir!\nI no oblidis d'esborrar els missatges!", reply_markup="")
    time.sleep(SETTINGS["SELF_DESTRUCT_SECONDS"])
    bot.deleteMessage(get_chat_id(update), MESSAGE_ID)
    return constants.END

# todo: document this class
class State:
    """Form State and main"""
    sender: Sender

    def __init__(self,
                 code,
                 query_builder: QueryBuilder,
                 next_state,
                 sender: Sender,
                 ):
        self.set(code, query_builder, next_state)
        self.sender = sender

    def set(self,
            code,
            query_builder: QueryBuilder,
            next_state,
            ):
        self.code = code
        self.qtype = "text" if isinstance(query_builder, TextQueryBuilder) else ""
        self.qtype = "buttons" if isinstance(query_builder, ButtonQueryBuilder) else self.qtype
        self.q = query_builder
        self.next_state = next_state

    def handler(self):
        if self.qtype == "text":
            return MessageHandler(Filters.text, self.callback, pass_user_data=True)
        elif self.qtype == "buttons":
            return CallbackQueryHandler(self.callback, pass_user_data=True)

    def query(self, bot, update, user_data):
        chat_id = get_chat_id(update)
        constants.info_log.info("Query for MESSAGE_ID="+str(MESSAGE_ID)+" AND chat_id="+str(chat_id)
                     + "  AND self.code="+str(self.code))
        constants.info_log.info(self.q.text, )

        # Transport Lines
        if self.code == WHERE:
            constants.info_log.info("Entering where")

            transport = user_data[WHICH]
            lines = list(constants.TRANSPORTS[transport]['lines'])
            line_matrix = vector2matrix(format_transport_lines(transport, lines), columns=5)
            # build a query that replaces the "dummy"
            self.q.buttons = ButtonQueryBuilder.button_matrix(line_matrix, add_all_button=True)
            bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text=self.q.text,
                                  reply_markup=InlineKeyboardMarkup(self.q.buttons))

        # Transport Stops
        elif self.code == WHERE2:
            constants.info_log.info("Entering WHERE2")
            if user_data[WHERE] == "Totes":
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text=self.q.text,
                                      reply_markup=InlineKeyboardMarkup(ButtonQueryBuilder.button_matrix([[("Continua", "Totes")]])))
            else:
                transport = user_data[WHICH]
                # We have to sanitize transport lines to fix color line editions on sanitize_lines
                line = constants.TRANSPORTS[transport]['lines'][sanitize_lines(user_data[WHERE])]

                stops = line['stops'] if 'stops' in line else ['En moviment', 'Al carrer (indica on)']
                stops_matrix = vector2matrix(stops)
                # build a query that replaces the "dummy"
                self.q.buttons = ButtonQueryBuilder.button_matrix(stops_matrix, add_all_button=True)
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text=self.q.text,
                                      reply_markup=InlineKeyboardMarkup(self.q.buttons))

        # Change text for questions
        elif self.code == HOW:
            constants.info_log.info("Entering HOW")
            if user_data[WHAT] == 'pregunta':
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text='Què vols saber?')
            elif user_data[WHAT] == 'incidencia':
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text='Explica el problema, sisplau')
            else:
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text=self.q.text)

        else:
            constants.info_log.info("Entering else")
            if self.qtype == "text":
                bot.edit_message_text(self.q.text, chat_id, MESSAGE_ID)
            elif self.qtype == "buttons":
                bot.edit_message_text(chat_id=chat_id, message_id=MESSAGE_ID, text=self.q.text,
                                      reply_markup=InlineKeyboardMarkup(self.q.buttons))

    def callback(self, bot, update, user_data):
        constants.info_log.info("Calling callback for MESSAGE_ID="+str(MESSAGE_ID) + " self.qtype=" +self.qtype)

        # fill the user_data message history array, joining messages coming
        # from written "text" and messages coming from pressed "buttons"
        if self.qtype == "text":
            user_data[self.code] = update.message.text
        elif self.qtype == "buttons":
            user_data[self.code] = update.callback_query.data
            update.callback_query.answer()

        if user_data[self.code] == "cancel":
            constants.info_log.info("calling cancel_callback with update {}".format(update))
            return cancel_callback(bot, update, user_data)

        if self.next_state is None:
            return complete_callback(bot, update, user_data, self.sender)
        self.next_state.query(bot, update, user_data)
        return self.next_state.code
