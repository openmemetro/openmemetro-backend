import logging
from telegram.ext import ConversationHandler
from enum import Enum
from .settings import SETTINGS
from .utils import setup_logger

EMOJIS = {
    'time_clock': '\u23F1',  # ⏱️ U+23F1
    'pushpin': '\U0001F4CC',
    'magnifyier': '\U0001F50E',  # 🔎 U+1F50E :mag_right:
    'train': '\U0001F683'  # 🚃 U+1F683 :railway_car:
}

class WarningObject(object):
    def __init__(self, icon, text) -> None:
        super().__init__()
        self.icon = icon
        self.text = text

class WARNING (WarningObject, Enum):
    # 'gorilas': '\U0001F620',  # 😠 U+1F620 :angry:
    GORILAS = '⚠🦺', 'Goril·les'  # 😠 U+1F620 :angry:
    # 'mosquitos': '\U0001F621',  # 😡 U+1F621 :rage:
    MOSQUITOS = '️📛🛂', 'Mosquits'  # 😠 U+1F620 :angry:
    # 'pitufos': '\u2614',       # ☔ U+2614  :umbrella:
    # 'gossos': '⚠\U0001F43A',         # 🐺 U+1F43A :wolf:
    GOSSOS= '⚠👮', 'Gossos'     # 🐺 U+1F43A :wolf:
    LLIURE= '\U0001F49A', 'Lliure' # 💚 U+1F49A :green_heart:
    INCIDENCIA= '\u2139', 'Info'  # ℹ U+2139   :information_source:
    PREGUNTA =  '\u2754', 'Pregunta' # ❔ U+2754  :grey_question:
    DEFAULT =   '\u26A0', 'Perill' # ⚠ U+26A0   :warning:


"""Shortcut for ConversationHandler.END"""
END = ConversationHandler.END

# Todo: this below are not constants, where to define it
# Read the lines and stop
TRANSPORTS = {}
# States dictionary
STATES = {}
# On memory black list
BLACK_LIST = {}

# Create two different loggers
setup_logger('warning', SETTINGS['WARNING_LOG'], logging.WARNING)
setup_logger('info', SETTINGS['INFO_LOG'])
warning_log = logging.getLogger('warning')
info_log = logging.getLogger('info')


