# Useful for building button matrices
import json
import logging
import os

from .settings import SETTINGS
from . import constants as constants

def format_metro_line(line):
    if line == "L1":    line = "🟥" + line
    elif line == "L2":  line = "🟪" + line
    elif line == "L3":  line = "🟩" + line
    elif line == "L4":  line = "🟨" + line
    elif line == "L5":  line = "🟦" + line
    elif "L9" in line:  line = "🔸" + line
    elif "L10" in line: line = "🔹" + line
    return line

def format_transport_lines(transport_type, transport_lines):
    """Used to give special text to transport line names

    For example, if the transport is metro and the line is named L1,
    prepend a red emoji as is the color of the line

    [transport_lines] could be treated as string to format a single
    line and not an array of lines
    """
    if transport_type == "Metro":
        if isinstance(transport_lines, str):
            transport_lines = format_metro_line(transport_lines)
        else:
            for i, line in enumerate(transport_lines):
                transport_lines[i] = format_metro_line(line)
    return transport_lines

def sanitize_lines(line_name):
    """What this conditional below does? Basically we implemented the function format_transport_lines that format
    metro lines to show the color of the line.
    This formated name is stored as user_data[WHERE] what is actually used to get the lines, so we have to
    look the line key on the json transpors. Basically is an ugly fix:
    For `user_data[WHERE] = "🟥L1"` we are going to find `"L1" in "🟥L1"`
    Basically is an ugly patch to give `format_transport_lines` support
    """
    return line_name[1:] if not line_name[0:1].isalnum() else line_name

def vector2matrix(vector: [], columns=3) -> [[]]:
    matrix = []
    row = []
    iv = iter(vector)

    while True:
        try:
            for j in range(columns):
                row.append(next(iv))

        except StopIteration:
            break

        finally:
            if row: matrix.append(row)
            row = []

    return matrix

def get_chat_id(update):
    if update.message:
        return update.message.chat_id
    elif update.callback_query:
        return update.callback_query.message.chat_id
    else:
        return None



def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='a')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)

def write_blacklist(blacklist):
    """Write on memory blacklist to the json file defined on settings"""
    with open(SETTINGS['BLACK_LIST'], 'w') as f:
        json.dump(blacklist, f, sort_keys=True)

def read_blacklist():
    """Read SETTINGS['BLACK_LIST'] file and set the content to BLACK_LIST variable

    If file is empty it initialize as json object
    """
    file_path = SETTINGS['BLACK_LIST']
    if not os.path.exists(file_path) or os.stat(file_path).st_size == 0:
        write_blacklist([])
    else:
        with open(file_path, 'r') as f:
            constants.BLACK_LIST = json.load(f)


