from telegram import ParseMode, Bot

from .constants import EMOJIS, WarningObject, WARNING, warning_log
from .settings import SETTINGS
from datetime import datetime, timedelta

from .utils import format_transport_lines


class Alert(object):
    when: datetime
    what: WARNING
    which: str
    where: str
    where2: str
    comments: str

    def __init__(self, when, what, which, where, where2, comments="", ):
        self.when = when
        self.what = what
        self.which = which
        self.where = format_transport_lines(which, where)
        self.where2 = where2
        self.comments = comments

    def format_msg(self, botname):
        '''
        Usually [which] is the kind of transport and [where] and [where2] the line/stop
        Comments could be empty
        What is one of the declared on WARNING enum
        '''
        msg = f"{self._getWhatFormatted(self.what)}\n" + \
              f"{EMOJIS['time_clock']} {self._getTimeFormated(self.when)} @{botname}\n" + \
              f"{EMOJIS['train']} {self.which} {EMOJIS['pushpin']} {self.where}: {self.where2}\n"
        if self.comments != "":
            msg += f"{EMOJIS['magnifyier']} {self.comments}"
        return msg

    def _getWhatFormatted(self, what: WarningObject):
        '''
        Just get the icon and the name and format it, example ⚠️👮 Gossos 👮⚠️
        '''
        return str(what.icon) + " **" + str(what.text).capitalize() + "** " + str(what.icon[::-1])

    def _getTimeFormated(self, when: datetime): return when.strftime('%H:%M')


class TelegramSender(object):
    def __init__(self, bot: Bot = None, token: str = None):
        self.bot = Bot(token) if bot is None else bot
        self.bot_name = self.bot.getMe()['username']

    def send_alert(self, alert: Alert):
        msg = alert.format_msg(self.bot_name)
        self.publish_message(msg)

    def publish_message(self, msg):
        for chat_id in SETTINGS["CHAT_IDS"]:
            self.bot.send_message(chat_id, msg, parse_mode=ParseMode.MARKDOWN)


class Sender(object):
    def __init__(self,
                 tg_sender: TelegramSender = None,
                 cb = None):
        self.tg_sender = tg_sender
        self.cb = cb

    def send_alert(self, alert: Alert, username: str, use_callback: bool = True):
        msg = alert.format_msg(self.tg_sender.bot_name)
        warning_log.warning(
            "Publish message: '{}'".format(msg) + "\n" + "Publisher: " + str(username))
        if self.tg_sender is not None: self.tg_sender.send_alert(alert)
        if self.cb is not None and use_callback: self.cb(alert)