#!/bin/python3
# coding=utf-8

from colatbot.colatbot import ColatBot
from colatbot.settings import SETTINGS


def main():
    bot = ColatBot(SETTINGS["TOKEN"])
    bot.start_polling()


if __name__ == "__main__":
    main()
