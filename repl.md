## Configure environment

Create the superuser and set the next variables:

```sh

user_name="admin"
password="1234"

```

## Static Data

```sh
curl -X GET http://localhost:8000/StaticData/
```

## Get cities

```sh

curl -X GET \
  -H "Authorization: Bearer $token" \
  http://localhost:8000/citys/ | jq

```

## Get countries

> todo: view not implemented yet

```sh

curl -X GET \
  http://localhost:8000/countrys/

```

## Get stations

```sh
curl -X GET \
  http://localhost:8000/stations/ | jq

```

## Get lines

```sh
curl -X GET \
  http://localhost:8000/lines/ | jq

```

## Synchronize

```sh

curl -X POST \
  -H "Authorization: Bearer  $token" \
  http://localhost:8000/synchronize/ | jq

```

## SignUp 

With `APP_DEBUG=true` on the `.env` the master captcha will be: `"captcha_key": "1234", "captcha_value" : "1234",`.

```sh
curl -X POST \
  -H "Content-Type: application/json" \
  -d '{
    "captcha_key": "1234",
    "captcha_value" : "1234",
    "username": "user",  
    "email": "test@test.es",  
    "password" : "test1234abcd",  
    "city_id": 1 
  }'  \
  http://localhost:8000/signup/

```


## Get Token

```sh
curl \
   -X POST \
   -H "Content-Type: application/json" \
   -d '{
    "captcha_key": "1234",
    "captcha_value" : "1234",
    "username": "user", "password": "test1234abcd"}' \
   http://localhost:8000/api/token/
```

* Associate token to `$token`

```bash
token=$(curl \
   -X POST \
   -H "Content-Type: application/json" \
   -d '{"username": "user", "password": "test1234abcd"}' \
   http://localhost:8000/api/token/ | jq -r '.access') \
&& echo $token
```

## Refresh token 

* Get refresh token

```bash
refresh=$(curl \
   -X POST \
   -H "Content-Type: application/json" \
   -d '{"username": "user", "password": "test1234abcd"}' \
   http://localhost:8000/api/token/ | jq -r '.refresh')
```

* Do refresh

```bash
curl \
   -X POST \
   -H "Content-Type: application/json" \
   -d '{ "refresh": "'$refresh'"}' \
   http://localhost:8000/api/token/refresh
```


## List Alerts

```sh
curl -X GET \
  http://localhost:8000/alerts/ | jq
```


## Add Alert

```sh
curl \
   -X POST \
   -H "Content-Type: application/json" \
   -H "Authorization: Bearer  $token" \
   -d '{"city_id": 1, "transport_id": 2, "comment" : "ola k ase", "line_id": 1,"station_id": 1 }' \
   http://localhost:8000/alert/
```


# Get Banner

```sh
curl -X GET \
  http://localhost:8000/banner/ | jq
```
