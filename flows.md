# Memetro
### Flujos de la aplicacion

#### Reportar alerta

### Endpoints encontrados en la app

#### Alerts:

* /alerts/listAlert

```json
  {
    "access_token "Meme",
    "city_id": 0,
  }
```

* /alerts/getTweets

```json
  {
    "access_token "Meme",
    "city_id": 0,
  }
```

* /alerts/deleteAlert
    
```json
  {
    "access_token "Meme",
    "id": 0,
  }
```

* /alerts/add

```json
  {
    "access_token "Meme",
    "station_id": 0,
    "line_id": 0,
    "city_id": 0,
    "transport_id": 0,
    "comment": 0,
  }
```

#### Synchronize

```json
  {
    "access_token "Meme",
  }
```

#### StaticData

```json
  {
    "client_id": "Meme",
    "client_secret": "Meme",
  }
```

#### Register

* /register/recoverPassword

```json

    {
      "access_token "Meme",
      "client_id": "Meme",
      "client_secret": "Meme",
      "email": "Meme",
    }
```

* /register/index
  
  OAuth.call("Register", "index", registerParams);

```json
  {
    "username": "",
    "password": "",
    "password2": "",
    "city_id": "",
    "name": "",
    "email": "",
    "tweeetername": "",
    "aboutme": "",
    "client_id": "Meme",
    "client_secret": "Meme",
  }
```  

  Devuelve:
  
```json  
  {
    "success": true,
    "message": "String",
    "error_code": "R005" // Error q devuelve "La contraseña debe tener mínimo 8 carácteres"
  }
```


#### Devices
* /devices/register

```json
  {
    "access_token": "",
    "type": "",
    "device": "",
  }
```

#### Users

* /users/edit_user_data

```json
  {
    "access_token": "",
    "name": "",
    "email": "",
    "city_id": "",
    "twittername": "",
    "aboutme": "",
  }
```

