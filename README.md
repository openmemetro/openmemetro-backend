# Memetro Api  #
Development repository to the memetro-api

### Production setup ###

Basically this is the brief of how to prepare you production enviorment to work, asuming you already cloned and created
the venv. You have to create a user with granted privileges for a database to use and to be configured on the `.env` var.

```bash
cd app/
python3 manage.py migrate
# Load fixtures, may you need to genereate it first with create_seed_data.py
python3 manage.py loaddata ./api/fixtures/*
# Create superuser for admin panel
python3 manage.py createsuperuser 
# Collect static data into static/ folder (such as admin panel stuff)
python3 manage.py collectstatic 
# Start submodules
git submodule update --init --recursive
git submodule update --recursive --remote
# If is the submodules first time execution, copy the colatbot default settings file, no prior configuration
cp colatbot/colatbot/settings.py.example colatbot/colatbot/settings.py 
```

Then fullify `.env` tih all required fields.

### How to start development enviorement ###

```
git clone git@gitlab.com:Dc23/memetro-api.git
cd memetro-api  
cp app/.env.example app/.env # You should fill the APP_KEY value

# Start docker
bin/build
bin/manage createsuperuser # Create superuser for the database
bin/start

# Maybe change app/db.sqlite3 ownership
sudo chown $USER: app/db.sqlite3
```

Visit [http://localhost:port](http://localhost:80) to check that is working
**port will be determined by APP_PORT value in .env**

### Fixtures data creation process

To create the seed data we use the `create_seed_data.py` script and the `transport.json` file placed in `docs` folder.

To create the fixtures files run:

```
$ python create_seed_data.py
```

Generates JSON files in `api/fixtures/`

### Execute manage.py in django ###

To execute manage.py in the django container you just have to execute **bin/manage (manage_parameter)** 

Example:
```
bin/manage makemigrations
```

### Authentication JWT

See `repl.md`.
